import { CartService } from './../../../core/services/cart.service';
import {
  Component,
  EventEmitter,
  Input, OnInit,
  Output,
  SimpleChange
} from '@angular/core';
import { Product } from 'src/app/core/models/product.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @Input()
  product!: Product;
  @Output() productClicked: EventEmitter<any> = new EventEmitter();

  today = new Date();

  constructor(private cartService: CartService) {
    // console.log('1. constructor')
  }

  ngOnInit(): void {
    // console.log('3. ngOnInit');
  }

  addCart() {
    console.log('anadido al carrito');
    this.cartService.addCart(this.product);
  }

}
