import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private af: AngularFireAuth) { }

  createUser(email: string, pass: string) {
    return this.af.createUserWithEmailAndPassword(email, pass);
  }

  login(email: string, pass: string) {
    return this.af.signInWithEmailAndPassword(email, pass);
  }
}
