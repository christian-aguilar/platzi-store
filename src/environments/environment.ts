// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url_api: "http://platzi-store.herokuapp.com",
  firebase: {
    apiKey: "AIzaSyAi74VV3ljaI8FVcCdlPKc7fZaVG_a6FnY",
    authDomain: "platzi-store-434c0.firebaseapp.com",
    projectId: "platzi-store-434c0",
    storageBucket: "platzi-store-434c0.appspot.com",
    messagingSenderId: "447602755616",
    // appId: "1:447602755616:web:366f460d1cc981c4ed1445"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
